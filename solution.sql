-- a. Find all artists that has a letter D
SELECT * FROM artists WHERE name LIKE '%D%';
-- b. Find all songs that has a length of less than 230
SELECT * FROM songs WHERE length < 230 ;
-- c. Join the albums and songs table (album name,song name, and song length)
SELECT albums.album_title, songs.song_name, songs.length
FROM albums
JOIN songs ON albums.id = songs.album_id;
-- d. Join the artists and albums tables ( find all albums that has letter A in its name)
SELECT artists.name, albums.album_title FROM artists
JOIN albums ON artists.id = albums.artist_id
WHERE albums.album_title LIKE '%A%';
-- e. sort the ablum in Z-A order ( show only the first 4 records)
SELECT album_title FROM albums ORDER BY album_title DESC LIMIT 4;
-- f. join the albums and songs table  ( sort albums from z-a and sort songs from a-z)
SELECT albums.album_title, songs.song_name FROM albums
JOIN songs ON albums.id = songs.album_id
ORDER BY albums.album_title DESC, songs.song_name ASC;